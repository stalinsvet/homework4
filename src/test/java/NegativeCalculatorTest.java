import model.Calculator;

import model.CalculatorException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class NegativeCalculatorTest {

    @DataProvider
    public Object[][] negativeData() {
        return new Object[][]{
                {"+", "76", "a"},
                {"-", "a", "76"},
                {"/", "5", "0"},
                {"*", "1.1", "76"},
                {"-", "3", "2.2"},
                {"-", "a", "76"},
                {"-", "a", "76"},
                {"-", "-2147483649", "0"},
                {"+", "2147483648", "0"},
        };
    }

    @Test(dataProvider = "negativeData", expectedExceptions = CalculatorException.class)
    public void negativeTest(String provider, String num1, String num2) {
        String actualResult = Calculator.execute(new String[]{provider, num1, num2});
    }
}

