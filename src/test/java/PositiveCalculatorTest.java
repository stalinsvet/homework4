
import model.Calculator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PositiveCalculatorTest {

    @DataProvider
    public Object[][] positiveData() {
        return new Object[][]{
                {"+", "54", "1", "55"},
                {"-", "73", "4", "69"},
                {"*", "2", "4", "8"},
                {"/", "76", "4", "19"}

        };
    }

    @Test(dataProvider = "positiveData")
    public void positiveTest(String provider, String num1, String num2, String result) {
        String expectedResult = result + "=" + num1 + provider + num2;

        String actualResult = Calculator.execute(new String[]{provider, num1, num2});
        Assert.assertEquals(expectedResult,  actualResult );
    }
}
