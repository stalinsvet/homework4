package model;

public class Calculator {
    public static String execute(String[] params) {

        String performedСalculations = null;
        try {
            int num1 = Integer.parseInt(params[1]);
            int num2 = Integer.parseInt(params[2]);
            int result;
            switch (params[0]) {
                case ("+"):
                    result = num1 + num2;
                    performedСalculations = result + "=" + num1 + "+" + num2;
                    break;
                case ("-"):
                    result = num1 - num2;
                    performedСalculations = result + "=" + num1 + "-" + num2;
                    break;
                case ("*"):
                    result = num1 * num2;
                    performedСalculations = result + "=" + num1 + "*" + num2;
                    break;
                case ("/"):
                    result = num1 / num2;
                    performedСalculations = result + "=" + num1 + "/" + num2;
                    break;
                default:
                break;
            }
        } catch (Exception e) {
            throw new CalculatorException("Неподходящее значение!!!");
        }
        return performedСalculations;
    }
}
