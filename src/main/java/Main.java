import model.Calculator;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Укажите сначала переменную: +, - ,* или/. " +"\n" +
                "После введите два любых целых числа." );
        String[] params = {scanner.next(),scanner.next(),scanner.next()};
        System.out.println(Calculator.execute(params));
    }
}
